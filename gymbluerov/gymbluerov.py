import gym
from gym.utils import seeding
from gym import spaces, logger
import scipy.integrate as scint
import numpy as np
from . import export


class Terminate(Exception):
    pass


def Rz(psi):
    """Rotation Matrix in the planar case"""

    return np.array([[np.cos(psi), -np.sin(psi), 0],
                     [np.sin(psi), np.cos(psi), 0],
                     [0, 0, 1]])


def Coriolis(nu, M):

    return np.array([[0, 0, -M[0, 0]*nu[1]],
                     [0, 0, M[0, 0]*nu[0]],
                     [M[0, 0]*nu[1], -M[0, 0]*nu[0], 0]])


class BROV(object):
    """
    BlueROV Simulator Class

    """
    def __init__(self, *args, **kwargs):
        """
        Args:
        DN (numpy.ndarray): Quadratic Damping Matrix
        DL (numpy.ndarray): Linear Damping Matrix
        MA (numpy.ndarray): Added Mass Matrix
        MR (numpy.ndarray): Rigid Body Mass Matrix
        """

        for key, val in kwargs.items():

            setattr(self, key, val)

        self.M = self.MA + self.MR
        self.iM = np.linalg.pinv(self.M)

    def EOM(self, time, state, args):
        """
        BlueROV Equation of Motion

        Args:
            time (float): simulator time
            state (numpy.ndarray): state vector
            args (tuple): additional arguments

        Returns:
            numpy.ndarray: differential state
        """
        eta = state[:3]
        nu = state[3:]
        tau = args[0]

        R = Rz(eta[-1])
        C = Coriolis(nu, self.M)
        DN = np.diag(self.DN.dot(abs(nu)))
        nudot = -(self.DL + C + DN).dot(nu) + tau

        etadot = R.dot(nu)
        return np.concatenate([etadot, self.iM @ nudot]).reshape((-1, 1))


@export
class BlueROVEnv(gym.Env):

    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self, *args, **kwargs):
        """
        BlueROV Environment

        Args:
            marker (numpy.ndarray): marker position in global frame
            dt (float): sampling time
            k1 (float): reward parameter 1
            k2 (float): reward parameter 2
            k3 (float): reward parameter 3
            tmax (float): maximum simulation time
        """
        # Marker pose must be specified
        if 'marker' not in kwargs.keys():
            raise KeyError('marker pose not specified')

        if 'dt' not in kwargs.keys():
            raise KeyError('sampling time not specified')

        if 'tmax' not in kwargs.keys():
            raise KeyError('Maximum simulation time not specified')

        for key, val in kwargs.items():
            setattr(self, key, val)

        # Action Space is Newton
        self.min_action = np.array([-100, -100, -1])
        self.max_action = -self.min_action
        # Position Constraints are the pool
        self.min_position = np.array([-20, -3])
        self.max_position = -self.min_position
        self.max_velocity = np.array([1.0, 1.0, 0.1])
        self.min_velocity = -self.max_velocity

        self.min_state = np.array(self.min_position.tolist() +
                                  [-np.pi] +
                                  self.min_velocity.tolist())
        self.max_state = -self.min_state

        self.observation_space = spaces.Box(low=self.min_state,
                                            high=self.max_state,
                                            dtype=np.float32)
        self.action_space = spaces.Box(low=self.min_action,
                                       high=self.max_action,
                                       dtype=np.float32)
        parameters = {
            'DL': np.diag([0, 0.26, 4.64]),
            'DN': np.diag([34.96, 103.25, 0.43]),
            'MA': np.diag([2.6, 18.5, 0.28]),
            'MR': np.diag([7.36, 7.36, 0.3])
        }

        vehicle = BROV(**parameters)
        self.world = scint.ode(vehicle.EOM).set_integrator('dopri5')

        self.seed()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):

        self.t += self.dt
        self.world.set_f_params((action,))

        while self.world.t < self.t:
            self.world.integrate(self.t)

        self._state = np.copy(self.world.y)

        done = self.terminal

        if not done:
            reward = self.reward
        elif self.steps_beyond_done is None:
            self.steps_beyond_done = 0
            reward = self.reward
        else:
            if self.steps_beyond_done == 0:
                logger.warn('You are calling step() even though this environment is done')
            self.steps_beyond_done += 1
            reward = -100.0

        return (self.relState, reward, self.terminal, {})

    @property
    def terminal(self):

        if self.t > self.tmax:
            return True

        # Perhaps add some Field-of-View conditions?
        if np.any(np.greater(self._state[:2], self.max_position[:2])):
            return True
        if np.any(np.less(self._state[:2], self.min_position[:2])):
            return True

        return False

    @property
    def reward(self):
        """
        Reward Function
        Returns:
            float: the reward of the current state
        """
        state = self.relState

        dPos = np.linalg.norm(state[:2])**2
        dVel = np.linalg.norm(state[3:5])**2
        return -(self.k1*dPos*(1 + self.k2*dVel) + self.k3*state[2]**2)

    @property
    def relState(self):

        relN = self._state[0] - self.marker[0]
        relE = self._state[1] - self.marker[1]
        relPsi = self._state[2] - self.marker[2]

        R = Rz(self._state[2])

        relPN = np.array([relN, relE, 0])

        relPB = np.linalg.inv(R).dot(relPN)
        x = np.array([relPB[0],
                      relPB[1],
                      relPsi,
                      self._state[3],
                      self._state[4],
                      self._state[5]])
        return x

    def reset(self):
        """
        Reset function for integrator

        Returns:
            numpy.ndarray: current state
        """
        self.t = 0
        self.steps_beyond_done = None
        try:
            self._state = self.initialize_state
        except NotImplementedError:
            logger.warn('State Initializer not implemented')
            self._state = np.array([-1, 0, np.pi/2, 0, 0, 0])
        self.world.set_initial_value(self._state)

        return self._state

    @property
    def initialize_state(self):

        raise NotImplementedError()


def main():

    env = BlueROVEnv(dt=0.05, marker=np.zeros(3), k1=1, k2=1, k3=1, tmax=30)

    env.reset()

    actions = np.array([15.4, 0.0, 0.0])

    done = False

    while not done:
        
        state, reward, done, _ = env.step(actions)

    print(state, reward)


if __name__ == '__main__':

    main()
