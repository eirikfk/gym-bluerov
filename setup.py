#!/usr/bin/env python

from setuptools import setup

setup(name='gymbluerov',
      version='0.0.1',
      description='',
      url='http://gitlab.com/mikcol/gym-bluerov.git',
      author='Mikkel Cornelius Nielsen',
      author_email='mikkel.cornelius@gmail.com',
      license='MIT',
      packages=['gymbluerov'],
      python_requires='>=3.6',
      install_requires=[
          'numpy',
          'scipy',
          'gym'
      ],
      zip_safe=False)
